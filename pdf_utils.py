"""
    - PDF downloader class
    - PDF merger class.
        Resolution of Invoice is 991 x 1403
        Resolution of Label:
            DHL: 703 x 992
            DPD: 496 x 698
        Pixels of A4 paper with DPI300: 2475 x 3525

"""
import base64
import glob
import json
import os
import urllib2

import time
from _ssl import SSLError

from PIL import Image, ImageDraw
# from wand.image import Image as WandImage
import PyPDF2 as pyPdf


try:
    import cups
except ImportError as e:
    print e

from base import Base


class PDFMerge(Base):
    pdf_file = 'downloaded.pdf'
    merged_file = ''
    merged_size = (842.0, 595.0)
    invoice_scale = 0.65        # scale of the label

    def __init__(self, pdf_file='downloaded.pdf'):
        Base.__init__(self)
        self.pdf_file = pdf_file
        self.merged_file = self.pdf_file.split('.')[0] + '_merged.pdf'

    def merge_pdf(self):
        input_pdf = pyPdf.PdfFileReader(file(self.pdf_file, "rb"))

        output = pyPdf.PdfFileWriter()

        # Create 1st page with the 1st Invoice and the Label(the last page)
        page = output.addBlankPage(self.merged_size[0], self.merged_size[1])
        page1 = input_pdf.getPage(0)
        page.mergeScaledTranslatedPage(page1, self.invoice_scale, 0, 30)

        page_invoice = input_pdf.getPage(input_pdf.getNumPages() - 1)
        label_size = page_invoice.mediaBox.upperRight
        print "Size of ", self.merged_file, " : ", label_size
        if float(label_size[0]) < 350:      # DPD
            scale = 0.95
            lx = 532
            ly = 120
        else:                               # DHL
            scale = 0.9
            lx = 480
            ly = 20
        page.mergeScaledTranslatedPage(page_invoice, scale, lx, ly)

        for i in range(1, input_pdf.getNumPages() - 1):
            page = output.addBlankPage(self.merged_size[0], self.merged_size[1])
            page.mergeScaledTranslatedPage(input_pdf.getPage(i), self.label_scale, 0, 0)

        # page.rotateClockwise(90)
        output_stream = file(self.merged_file, "wb")
        output.write(output_stream)
        output_stream.close()
        return True


class PDFDownloader(Base):

    def __init__(self):
        Base.__init__(self)

    def download_to_file(self, dest_file=None):
        req = urllib2.Request(self.get_param_from_xml('API_URL'))
        req.add_header('Accept', '  ')
        req.add_header('X-API-TOKEN', self.get_param_from_xml('TOKEN'))
        req.add_header('accept-encoding', 'gzip, deflate')

        try:
            resp = urllib2.urlopen(req, timeout=10)
        except urllib2.URLError:
            print 'Failed to download, connection lost.'
            return None
        except SSLError as e:
            print e
            return None

        content = json.loads(resp.read())
        if 'message' in content.keys():
            if content['message'] == 'Authentication failed.':
                print 'Authentication failed.'
                return None

        data = dict()
        data['count'] = content['count']
        data['extra'] = content['extra']
        data['data'] = []
        for i in range(len(content['data'])):
            tmp = dict()
            tmp['id'] = content['data'][i]['id']
            tmp['mode'] = content['data'][i]['mode']
            tmp['created_at'] = content['data'][i]['created_at']
            tmp['delivered_at'] = content['data'][i]['delivered_at']

            if dest_file is None:
                pdf_file = 'downloaded_' + str(i) + '.pdf'
            elif dest_file[-3:] != '.pdf':
                pdf_file = dest_file + '_' + str(i) + '.pdf'
            buf = base64.b64decode(content['data'][i]['filestream'])
            open(pdf_file, 'wb').write(buf)

            tmp['pdf_file'] = pdf_file

            data['data'].append(tmp)

        print 'Succeeded to get data: ', data.items()

        return data


def print_result(p_file):

    conn = cups.Connection()
    printers = conn.getPrinters()

    try:
        printer_name = printers.keys()[0]
    except KeyError:
        print "No printer is detected..."
        return False
    except IndexError:
        print "No printer is detected..."
        return False

    print "Printing file: ", p_file, " with ", printer_name

    print "Printing, return value: ", conn.printFile(printer_name, p_file, 'Printing result', {})


if __name__ == '__main__':

    # s_time = time.time()
    # t = PDFDownloader()
    #
    # data = t.download_to_file()
    # print "Elapsed: ", time.time() - s_time
    #
    # for received_data in data['data']:
    #     a = PDFMerge(pdf_file=received_data['pdf_file'])
    #     if received_data['mode'] != 'regular':
    #         a.merge_pdf()
    #         received_data['pdf_file'] = a.merged_file

        # print_result(received_data['pdf_file'])

    # a = PDFMerge(pdf_file='downloaded_0.pdf')

    p_list = glob.glob('download/*.pdf')
    for p in p_list:
        a = PDFMerge(pdf_file=p)
        a.merge_pdf()



