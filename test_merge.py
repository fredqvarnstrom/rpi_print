import PyPDF2 as pyPdf


def mergePdfs(filename1, filename2):
    file1 = pyPdf.PdfFileReader(file(filename1, "rb"))
    file2 = pyPdf.PdfFileReader(file(filename2, "rb"))

    output = pyPdf.PdfFileWriter()
    page = output.addBlankPage(842, 595)

    page1 = file1.getPage(0)
    page2 = file2.getPage(0)

    page.mergeScaledTranslatedPage(page1, 0.7, 0, 0)
    page.mergeScaledTranslatedPage(page2, 0.8, 550, 150)

    # page.rotateClockwise(90)
    outputStream = file("pdf/merged.pdf", "wb")
    output.write(outputStream)
    outputStream.close()
    return True


if __name__ == '__main__':

    mergePdfs("pdf/invoice.pdf", "pdf/label.pdf")



