import subprocess

"""
Python wrapper to manage connect/disconnect wifi

- Message when succeeded to connect:

    Internet Systems Consortium DHCP Client 4.3.1
    Copyright 2004-2014 Internet Systems Consortium.
    All rights reserved.
    For info, please visit https://www.isc.org/software/dhcp/

    Listening on LPF/wlan0/b8:27:eb:31:2c:07
    Sending on   LPF/wlan0/b8:27:eb:31:2c:07
    Sending on   Socket/fallback
    DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 4
    DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 5
    DHCPREQUEST on wlan0 to 255.255.255.255 port 67
    DHCPOFFER from 192.168.1.1
    DHCPACK from 192.168.1.1
    bound to 192.168.1.161 -- renewal in 3299 seconds.

- When fail:

    Internet Systems Consortium DHCP Client 4.3.1
    Copyright 2004-2014 Internet Systems Consortium.
    All rights reserved.
    For info, please visit https://www.isc.org/software/dhcp/

    Listening on LPF/wlan0/b8:27:eb:31:2c:07
    Sending on   LPF/wlan0/b8:27:eb:31:2c:07
    Sending on   Socket/fallback
    DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 6
    DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 12
    DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 13
    DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 15
    DHCPDISCOVER on wlan0 to 255.255.255.255 port 67 interval 15
    No DHCPOFFERS received.
    No working leases in persistent database - sleeping.

"""

import wifi


def connect_to_ap(dest_ssid, passkey):
    """
    Connect to the AP with given SSID & password
    :param dest_ssid: SSID of destination AP
    :param passkey:   password
    :return:
    """

    l_cell = wifi.Cell.all('wlan0')
    dest_cell = None
    for cell in l_cell:
        if cell.ssid == dest_ssid:
            dest_cell = cell
            break

    if dest_cell is None:
        print "Failed to discover given AP"
        return False

    scheme = wifi.Scheme.for_cell('wlan0', name=dest_cell, cell=dest_cell, passkey=passkey)
    scheme.save()
    try:
        scheme.activate()
        return True
    except wifi.exceptions.ConnectionError as e:
        print e
        return False


def get_ap_list():
    l_cell = wifi.Cell.all('wlan0')
    ap_list = []
    for cell in l_cell:
        ap_list.append(cell.ssid)
    return ap_list


if __name__ == '__main__':

    print get_ap_list()

    connect_to_ap('TP-LINK_REAL3D', 'dkanehahffk1')




