import sys

try:
    import cups
except ImportError:
    print "Failed to import CUPS package"


class RPiPrinter:

    conn = None

    def __init__(self):
        try:
            self.conn = cups.Connection()
        except NameError:
            self.conn = None

    def get_printer_list(self):
        """
        Get dict of discovered CUPS printers
        """
        l_printer = self.conn.getPrinters()
        for p in l_printer:
            print p, ":   ", l_printer[p]['device-uri']

        return l_printer.keys()

    def print_file(self, p_name, file_path):
        """
        Print file with a printer
        :param file_path: Full path of destination file
        :param p_name: Printer name
        """
        print "Printing ", file_path, "   with ", p_name
        return self.conn.printFile(p_name, file_path, 'Printing file', {})

if __name__ == '__main__':
    c = RPiPrinter()
    if len(sys.argv) > 1:
        pdf_file = sys.argv[1]
        c.print_file(c.get_printer_list()[0], pdf_file)
